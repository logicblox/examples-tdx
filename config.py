from lbconfig.api import *

lbconfig_package('application', version='0.1',
                 default_targets=['start-services'])

depends_on(logicblox_dep, lb_web_dep)

lb_library(name='application', srcdir='.', deps = ['lb_web'])

check_lb_workspace(name='application', libraries=['application'])

rule('start-services', ['check-ws-application'], [
  'lb web-server load-services'
], True)
